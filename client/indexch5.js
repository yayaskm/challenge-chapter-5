const express = require('express')
const app = express()
const port = 3000
const path = require('path') 



//view engine

//built-in middleware
app.use(express.static('public'));

app.get("/", function(req,res) {
    res.sendFile(path.join(__dirname+'/index.html'))
})


app.get("/play", function(req, res){
    res.sendFile(path.join(__dirname+'/public/index-challenge-4.html'))
})

app.get("/login", function (req, res, next) {
    res.sendFile(path.join(__dirname+'/login.html'));
})

app.use('/',(req,res) =>{
    res.status(404);
    res.status(500);
    res.send('<h1>Oops halaman ini sedang error!</h1>');
})

app.listen(port,() => console.log(`Running on http://localhost:${port}`))